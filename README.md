# Pastas de referencia

  - "L:\# ipeadata_lab #"  , pasta aberta (para leitura para) qualquer usuário do Ipea. Os membros do IpeaDATA-lab tem permissão de escrita também. Usamos esta pasta para: compartilhar arquivos; manter dados públicos de pesquisas (que  são muito grnades para o gitlab); manter repositório de softwarees ferramentas e manuais; manter histório dos acordos de dados, manter metadados das várias bases de dados (no futuro mudar isso de lugar),  etc
  - "\\storage6\bases\restrito" , pasta com dados sigilosos, recebidos desde dez/2016
  - "\\storage4\bases\restrito" , pasta com dados sigilosos, tradicional do Ipea exite há muitos anos, contêm dados da RAIS
  - "\\storage2\bases\restrito"pasta com dados sigilosos, recebidos desde dez/2016
  - "\\storage4\bases\Batimentos", pasta com dados dos batimentos de programas sociais e do BPC. Alguns bolsistas tem acesso. 
 
# Servidores de processamento de dados e máquinas virtuais
  - Servidores Estatísticos Tradicionais: 
      - sstata2    : 512 Gb (servidor antigo) . Tem R e Stata. usado por todo o Ipea. Não deixar programas abertos monopolizando a memória (está sempre cheio devido à tragédia dos comuns)
      - sstata3    : 512 Gb (servidor novo, mais rapido). Tem R e Stata. Atualmente (jun/2018) usado apenas em alguns projetos do Ipeadata-lab
      - sbsb35     : 256 Gb. Tem R, Stata, Postgresql/Postgis, Spark (windows). Usado pelo IpeaDATA-lab e pela equipe do Alexandre Ywata.
  - Ambiente de sigilo
      - srestrito1 : 256 Gb. Tem R. Servidor restrito, não acessa a internet. Entrada e saida de dados controlada pela chefia
  - Servidores de Big-Data, Spark
      - XXX completar aqui  ###
  - Outros:
      - sbsb32: qgis , PgAdmin (client do PostgreSQL/Postgis) 
      - sbsb33: Abby Fine Reader 11 (OCR) 
      - postgresql-pesquisa (10.1.2.47), porta 5432, Servidor com Postgresql/Postgis. Lá dentro temos alguns bancos importantes: data_lake, dados_espaciais
      - sbsb8-pesquisa : Servidor com o SQL Server 2012 (talvez seja 2008)

# Pedidos administrativos

  - Criação de login e senha: pedir para asssitente administrativo (Guilherme, ramal 5553)
  - instalação de software: por e-pedidos


# Algumas referências iniciais (melhorar no futuro)

#### Estudar R:
  - O que vocês tem usado?
  - Usem sempreo Rstudio como IDE, facilita muito
  - Acho que uma boa fonte é: www.datacamp.com/home , façam os níveis que tiver e que forem grátis
  - Outra opção: http://swirlstats.com/students.html 
  - O Nicolas também sugeriu o http://r4ds.had.co.nz/ , mas é um  livro grande e cheio de detalhes. Não pirem demais nele
  - Para escrever pacotes de R a melhor referência é: http://r-pkgs.had.co.nz/
  - Fora isso, estudem os pacotes que mais usamos:
  - Tidyverse
  - Principalmente dplyr, tidyr, aprendam a usar o piping ( %>%)
  - Tentem entender os comandos neste cheat sheet: https://www.rstudio.com/wp-content/uploads/2015/02/data-wrangling-cheatsheet.pdf
  - data.table

#### Boas Práticas de Programação para Pesquisa Aplicada (Gustavo e Tamara)

 1. Leiam os dois textos abaixo e o vídeo abaixo, que cobrem vários dos tópicos acima, e tem boas práticas de programação: 
  - https://web.stanford.edu/~gentzkow/research/CodeAndData.pdf
  - http://faculty.chicagobooth.edu/matthew.gentzkow/research/ra_manual_coding.pdf (link não encontrado)
  - http://www.nber.org/econometrics_minicourse_2013/   (assistam apenas ao último vídeo) 

 1. Controle de versão :
  - Criem Contas no github.com e  gitlab.com e  me avisem.
  - Com isso vou solicitar acesso para vocês a alguns projetos (microdadosBrasil, validaRA, batimentosde bases de dados , parar que vocês possam contribuir com eles.
  - Aprendam a usaro Git, o mais fácil é ver os capítulos relevantes sobre isso  em  http://r-pkgs.had.co.nz/
	
#### Stata (não obrigatório, apenas se acharem útil)
  - Disponibilizei meu curso em L:\# ipeadata_lab #\BMAP\Referências\curso_Stata_Lucas_Mation

#### Preencha a tabela de contato e horários
  - Acesse o link abaixo e preencha suas informações de contato e horários.
  - https://docs.google.com/spreadsheets/d/1KjqId01QL3WJ8wSaw-he0jZk7UKbnm1dadRRUFOjnUs/edit#gid=0 
